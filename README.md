# jburrows001

## Jeff README

**[Jeff Burrows - Senior Manager, Security Compliance]** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

* GitLab profile: [https://gitlab.com/jburrows001](https://gitlab.com/jburrows001)
* My LinkedIn profile: [https://www.linkedin.com/in/jeff-burrows-security/](https://www.linkedin.com/in/jeff-burrows-security/)

## About me

* I started my career in non-profit accounting and operations before getting my MBA and switching to the security industry.
* My hobbies include:
   * Adventuring with my family
   * Running
   * Woodworking
   * Cooking
* I'm particularly interested in these GitLab sub-values:
   * [No ego](/handbook/values/#no-ego) - I think being vulnerable and getting in the habit of admitting when you are wrong makes it easy for others to challenge assumptions and bias that creep into work which increases the quality of my results.
   * [Measuring results and not hours](/handbook/values/#measure-results-not-hours) - I think this is something that all organizations struggle with but staying committed to this as an organization helps solve a lot of foundational problems most large companies experience.
   * [Don't let each other fail](/handbook/values/#dont-let-each-other-fail) - I believe in the expression "a rising tide lifts all boats" and don't see work as zero sum. I want everyone around me to succeed since that helps GitLab and all team members so I try and make time to support other team members as much as possible.

## How you can help me

* Follow up with a desired response/completion date if I haven't responded to your issue tag in GitLab. I use Slack messages and my email inbox as my task list and while I think I have a very strong completion rate of all issues I'm tagged in or requests sent via slack, sometimes I perceive things to be a lower priority than other issue tags or slack messages. If you think I haven't responded to something important in a reasonable amount of time, I never take offense to a follow up tag with a target time/date of when you need a response or result from me. I can always push back if I don't think I can accomodate.
* Provide me with blunt feedback. I've only been a people manager for 7 years and in the security compliance industry for 9 years so there are lots of areas I can/want to improve upon. I have a very thick skin and if you think I'm failing you or any GitLab team, please send me a direct message in Slack or let my boss know. I try and do a very good job of treating this type of information as an opportunity to improve my skillset and don't take offense.
* Tell me how Security Compliance as a topic feels to you within GitLab. The whole of Security Compliance teams are trying hard to build a better type of security compliance program than most companies have. We don't want things to feel as though our team is just trying to check boxes or impose unecessary requirements. We should have a very direct answer any time you ask "Why are we doing this?". In order to build the best possible Security Compliance program at GitLab, we need feedback on the ways in which our program feels better/worse/different.

## My working style

* I prefer direct communication
* I like constraints and requirements (e.g. It is easier for me to prioritize a task when I know why that task is needed and a date by which you want that task completed)
* I like taking big topics and breaking them into the following chunks:
   * What is this thing?
   * Why are we doing this thing?
   * What constraints and requirements are involved in this thing?
   * What are the major phases of work and milestones and when do we expect to complete those phases and milestones?
   * What are the tangible next steps to get us from where we are today to the end of all phases and milestones?
* I welcome MR's to this section (and all sections) of this README since I think others would have great insight into my working style

## What I assume about others

* I strive for 100% follow through on all tasks assigned to me so I tend to have a high bar for others in this area as well. In my mind when I ask someone to do something or they volunteer for a task, the implication in my mind is that I no longer need to think about that task and I can assume it will get done.
* I come from a business background so I see a lot of value in metrics and data and turning data into information that is accessible by others. This can manifest as me seeking strong planning documentation from others which might not be the way other people process that same work.

## What I want to earn

* The trust and respect of the GitLab security team
* A comprehensive knowledge of what systems make up GitLab as a company
* The belief of all GitLab team-members that the GitLab Security Compliance program is the best Security Compliance program you have worked with in your career
* A sense of expertise of people management within the Security Compliance industry

## Communicating with me

* I am strongly in favor of async communication but I ackowledge that Security Compliance can be a complex topic so I also value any video or in-person communication when we haven't worked together before on a difficult topic. I think my curiosity and empathy comes across best with visual cues.
* I normally work between 7:30am and 4:30pm Pacific Time Monday through Friday with exceptions when life events pop up. 

## Strengths/Weaknesses

* Strengths:
   * I am strong with program planning and documentation
   * I have very strong follow through on tasks assigned to me (i.e. very few issues I'm tagged in "slip between the cracks")
   * I think I do a good job of priorizing the most important work
   * I approach my work with empathy and genuine interest
   * I do a good job of relating Security Compliance needs to business objectives and customer interest
* Weaknesses:
   * I can struggle to think about things from an engineer's mindset since my career started in the business
   * I don't always do a good job of communicating that I am de-prioritizing something and why. This can lead a backlog of work others might think I'm ignoring when I'm really just over-capacity and priotizing work I see as more important/impactful/urgent
